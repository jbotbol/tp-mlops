import pytest
import os
import joblib
import pandas as pd

from sklearn.metrics import accuracy_score

dir_path = os.path.dirname(os.path.realpath(__file__))


@pytest.fixture
def model():
    path = dir_path + "/../models/titanic.pkl"
    s=0
    model = joblib.load(path)  # pytest metafunc
    yield model
    assert os.path.exists(path)  # assert we did not remove it


@pytest.fixture
def dataset():
    path = dir_path + "/../data/test.csv"
    df = pd.read_csv(path, index_col="PassengerId")
    yield df.drop("Survived", axis=1), df["Survived"]
    assert os.path.exists(path)


def test_accuracy(model, dataset):
    X, y_true = dataset
    y_pred = model.predict(X)
    assert accuracy_score(y_pred, y_true) > 0.6
